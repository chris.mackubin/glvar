import argparse
import json
import requests
from requests.exceptions import HTTPError

parser = argparse.ArgumentParser(prog='glvar', description='Update Gitlab project level variables.')
parser.add_argument('project', type=str, help='Project number or path')
parser.add_argument('key', type=str, help='Key of the variable (variable name)')
parser.add_argument('value', type=str, help='Value to be placed in variable')
parser.add_argument('--scope', type=str, help='The environment scope of the variable')
parser.add_argument('--token', type=str, required=True, help='Private access token (required)')
parser.add_argument('-f', action='store_true', help='Variable stored as a file instead of an environment variable.')
parser.add_argument('-p', action='store_true', help='Variable will be protected.')

args = parser.parse_args()

headers = {'Content-Type': 'application/json', 'Accept': 'application/json', 'PRIVATE-TOKEN': args.token}
url_base = 'https://gitlab.com/api/v4/projects/' + args.project + '/variables/' + args.key
var_type = 'file' if args.f else 'env_var'
var_protect = 'true' if args.p else 'false'

try:
    
    # Strip off matching quotes from value arg
    if args.value.startswith(("'", '"')) and args.value.endswith(("'", '"')):
        args.value = args.value[1:-1]
    print('Update value to : {0}'.format(args.value))

    # Look for existing variable
    if args.scope:
        response = requests.get(url_base + '?filter[environment_scope]=' + args.scope, headers=headers)
    else:
        response = requests.get(url_base, headers=headers)

    if response.status_code == 409:
        print('Gitlab project variable {0} not updated'.format(args.key))
        print(response.text)

    # Variable doesn't exist
    if response.status_code == 404:
        new_data = '{"key": "' + args.key + '", "value": "' + args.value + '", "variable_type": "' + var_type + '", "environment_scope": "' + args.scope + '", "protected": ' + var_protect + '}'
        response = requests.post('https://gitlab.com/api/v4/projects/' + args.project + '/variables', data=new_data, headers=headers)

        if response.status_code == 201:
            print('Gitlab project variable {0} has been created'.format(args.key))

        response.raise_for_status()

    else:

        # Update existing variable
        response.raise_for_status()
        data = json.loads(response.content)

        # Check if value has changed
        if data['value'] == args.value and data['variable_type'] == var_type and data['protected'] == var_protect:
            print('Gitlab project variable {0} value has not changed, No Action'.format(args.key))
        else:
            new_data = '{"value": "' + args.value + '", "variable_type": "' + var_type + '", "protected": ' + var_protect + '}'
            if args.scope:
                response = requests.put(url_base + '?filter[environment_scope]=' + args.scope, data=new_data, headers=headers)
            else:
                response = requests.put(url_base, data=data, headers=headers)

            if response.status_code == 200:
                print('Gitlab project variable {0} has been updated'.format(args.key))

            response.raise_for_status()

except HTTPError as http_err:
    print('HTTP error occurred while attempting to update Gitlab project variable {0}: {1}'.format(args.key, http_err))
except Exception as err:
    print('Error occurred while attempting to update Gitlab project variable {0}: {1}'.format(args.key, err))
